<?
	
namespace Vldud\CastImport\FileHandler;
	
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;	
	
class SpoutFileHandler extends FileHandler
{

    public $xmlData;

    public function read($file)
    {
        require_once $_SERVER['DOCUMENT_ROOT']. '/local/classes/Spout/Autoloader/autoload.php';
        if(file_exists($file)){
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($file);
            $this->xmlData = $reader;
        }
    }

}
?>