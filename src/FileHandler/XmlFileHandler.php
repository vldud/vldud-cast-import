<?
	
namespace Vldud\CastImport\FileHandler;
	
class XmlFileHandler extends FileHandler
{

    public $xmlData;

    public function read($file)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $file);
        $result = curl_exec($ch);
        curl_close($ch);
        $this->xmlData = simplexml_load_string($result);
    }

}
?>