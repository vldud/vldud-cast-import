<?
	
namespace Vldud\CastImport\FileHandler;
	
abstract class FileHandler
{

    abstract public function read($file);

    public function getFileFromDir($dir)
    {
        $arDir = scandir($dir);
        foreach ($arDir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                $fileInfo = new \SplFileInfo($value);
                $extension = $fileInfo->getExtension();
				$extension = strtolower($extension);
                if ($extension == "xls" || $extension == "xlsx") {
                    return $dir . $value;
                }
            }
        }
        return false;
    }

}
?>