<?
	
namespace Vldud\CastImport\FileHandler;
	
class PHPExcelFileHandler extends FileHandler
{

    public $xmlData;

    public function read($file)
    {
        require_once("PHPExcel/IOFactory.php");

        $objPHPExcel = PHPExcel_IOFactory::load($file);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $this->xmlData = array();
        for ($row = 1; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE
            );
            $this->xmlData[] = $rowData;
        }
    }

}	
?>