<?
	
namespace Vldud\CastImport\DBHandler;
	
abstract class DBHandler
{

    abstract public function getProductArray($data);

    abstract public function importProduct($arData);

    abstract public function getCursor();

    abstract public function setCursor($cursor = 0);

}	
?>