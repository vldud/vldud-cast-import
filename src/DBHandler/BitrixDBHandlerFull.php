<?
	
namespace Vldud\CastImport\DBHandler;

use \CIBlockSection as CIBlockSection;
use \CIBlockElement as CIBlockElement;
use \COption as COption;
use \CPrice as CPrice;
use \Cutil as Cutil;
use \CFile as CFile;
use \CCatalogProduct as CCatalogProduct;
	
class BitrixDBHandlerFull extends DBHandler
{
	
    private $cursorModuleName;
    private $cursorName;
    private $arSections;
    private $arManufacturers;
    private $importToSection;
    private $markup;

    function __construct($config = array())
    {
        $this->cursorModuleName = (isset($config["cursorModuleName"])) ? $config["cursorModuleName"] : "tega.import";
        $this->cursorName = (isset($config["cursorName"])) ? $config["cursorName"] : "import_cursor";
        $this->importToSection = (isset($config["importToSection"])) ? $config["importToSection"] : false;
        $this->markup = (isset($config["markup"])) ? $config["markup"] : -5;
        $this->arSections = $this->getSectionsList();
        $this->arManufacturers = $this->getManufacturersList();
    }

    private function errorOutput($strMessage)
    {
        // echo "<pre>";
        // print_r($strMessage);
        // echo "</pre>";
        AddMessage2Log($strMessage);
    }

    public function getCode($str)
    {
        $code = Cutil::translit(
            $str,
            "ru",
            array(
                "replace_space" => "-",
                "replace_other" => "-"
            )
        );
        return $code;
    }

    private function getManufacturersList()
    {
		$arResult = array();
        $res = CIBlockElement::GetList(
            Array(),
            Array(
                "IBLOCK_ID" => 14
            ),
            false,
            false,
            Array("ID", "NAME")
        );
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arResult[$arFields['NAME']] = $arFields['ID'];
		}
        return $arResult;
    }	
	
    private function getSectionsList()
    {
        $arResult = array();
        $arFilter = array(
            'IBLOCK_ID' => 16,
            'DEPTH_LEVEL' => 1
        );
        $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
        while ($arSection = $rsSections->Fetch())
        {
            $arResult[$arSection['NAME']] = $arSection['ID'];
        }
        return $arResult;
    }

    private function getSectionID($strSectionName)
    {
        if($this->importToSection != false){
            return $this->importToSection;
        }
        if(!is_array($this->arSections)){
            $this->errorOutput('Could not create array of sections');
        }
        if(array_key_exists($strSectionName, $this->arSections)){
            return $this->arSections[$strSectionName];
        } else {
            $bs = new CIBlockSection;
            $arFields = Array(
                "ACTIVE" => "Y",
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => 16,
                "NAME" => $strSectionName,
                "CODE" => $this->getCode($strSectionName)
            );
            $ID = $bs->Add($arFields);
            $res = ($ID>0);
            if($ID <= 0){
                $this->errorOutput('Error creating section ' . $strSectionName . ":" . $bs->LAST_ERROR);
                return false;
            } else {
                $this->arSections[$strSectionName] = $ID;
                return $ID;
            }
        }
    }

    public function getProductArray($data)
    {
        $sectionId = $this->getSectionID($data[17]);
        if($sectionId == false){
            return false;
        } else {
            $arData = array();
            $arProps = array();

            $arProps["100"] = $data[0];
            $arProps["51"] = $data[1];
            $arProps["87"] = $data[1];
            $arProps["88"] = $data[2];
            $arProps["89"] = $data[3];
            $arProps["71"] = $data[5];
            $arProps["90"] = $data[6];
            $arProps["91"] = $data[8];
            $arProps["92"] = $data[9];
			
			if($data[10]!=""){			
				if(array_key_exists($data[10], $this->arManufacturers)){
					$arProps["52"] = $this->arManufacturers[$data[10]];
				}
			}
			
            $arProps["93"] = $data[11];
            $arProps["94"] = $data[12];
            $arProps["95"] = $data[13];
            $arProps["96"] = $data[14];
            $arProps["97"] = $data[15];
            $arProps["98"] = $data[16];
            $arProps["99"] = $data[17];
            $arProps["101"] = 35;

            $arData["IBLOCK_ID"] = 16;
            $arData["QUANTITY"] = $data[2];
            $arData["ACTIVE"] = 'Y';
            $arData["CODE"] = $this->getCode($data[7] . " " . $data[0]);
            $arData["IBLOCK_SECTION_ID"] = $sectionId;
            $arData["XML_ID"] = $data[0];
            $arData["PROPERTY_XML_ID"] = $data[0];
            $arData["PRICE"] = $data[4];
            $arData["NAME"] = $data[7];
            $arData["PROPERTY_VALUES"] = $arProps;

            if($data[1] != ""){
                $arExtensions = array(
                    "jpg",
                    "jpeg",
                    "gif",
                    "png"
                );
                $picturePath = "";
                foreach($arExtensions as $ext){
                    if(file_exists($_SERVER['DOCUMENT_ROOT'] . "/upload/import_photo/" . $data[1] . "." . $ext)){
                        $picturePath = $_SERVER['DOCUMENT_ROOT'] . "/upload/import_photo/" . $data[1] . "." . $ext;
                        break;
                    }
                }
                if($picturePath != ""){
                    $arData["DETAIL_PICTURE"] = CFile::MakeFileArray($picturePath);
                }
            }

            return $arData;
        }
    }

    private function getPrice($price)
    {
        $markup = floatval($this->markup);

        $price = str_replace(" ", "", $price);
        $price = str_replace(",", ".", $price);
        $price = $price * (100 + $markup) / 100;
        /*$price = round($price, 2);*/
        $price = round($price);
        return floatval($price);
    }

    public function importProduct($arData)
    {
        $arProductArray = $arData;

        $res = CIBlockElement::GetList(
            Array(),
            Array(
                "IBLOCK_ID" => $arData["IBLOCK_ID"],
                "PROPERTY_XML_ID" => $arData["PROPERTY_XML_ID"]
            ),
            false,
            Array("nPageSize" => 1),
            Array("ID", "NAME")
        );
        $el = new CIBlockElement;
        $ob = $res->GetNextElement();
        if (!$ob) {
            $PRODUCT_ID = $el->Add($arProductArray);
            if ($PRODUCT_ID == 0) {
                $this->errorOutput('Error creating element "' . $arProductArray["NAME"] . '": ' . $el->LAST_ERROR);
            }
        } else {
            $arFields = $ob->GetFields();
            $PRODUCT_ID = $arFields["ID"];
            $el->Update($PRODUCT_ID, array("DETAIL_PICTURE" => $arProductArray["DETAIL_PICTURE"]));
			CIBlockElement::SetPropertyValuesEx(
				$arFields["ID"],
				$arFields["IBLOCK_ID"],
				array("UPDATED_BY_IMPORT_SCRIPT" => 35)
			);
			if(!empty($arProductArray["PROPERTY_VALUES"]["51"])){
				CIBlockElement::SetPropertyValuesEx(
					$arFields["ID"],
					$arFields["IBLOCK_ID"],
					array("ARTNUMBER" => $arProductArray["PROPERTY_VALUES"]["51"])
				);
			}
            $minPrice = sprintf("%.2f", $this->getPrice($arData["PRICE"]));
            CIBlockElement::SetPropertyValuesEx(
                $arFields["ID"],
                $arFields["IBLOCK_ID"],
                array("MINIMUM_PRICE" => $minPrice)
            );
        }

        if ($PRODUCT_ID > 0) {
            if (CCatalogProduct::Add(
                array(
                    "ID" => $PRODUCT_ID,
                    "VAT_INCLUDED" => "Y",
                    "QUANTITY" => intval($arData["QUANTITY"]),
                    "AVAILABLE" => "Y"
                )
            )
            ) {
                $arFields = Array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => 1,
                    "PRICE" => $this->getPrice($arData["PRICE"]),
                    "CURRENCY" => "RUB"
                );
                $res = CPrice::GetList(
                    array(),
                    array(
                        "PRODUCT_ID" => $PRODUCT_ID,
                        "CATALOG_GROUP_ID" => 1
                    )
                );
                if ($arr = $res->Fetch()) {
                    CPrice::Update($arr["ID"], $arFields);
                } else {
                    CPrice::Add($arFields);
                }
            } else {
                $this->errorOutput('Error updating product properties, ID' . $PRODUCT_ID);
            }
        }
    }

    public function getCursor()
    {
        return COption::getOptionString($this->cursorModuleName, $this->cursorName, 1);
    }

    public function setCursor($cursor = 1)
    {
        return COption::SetOptionString($this->cursorModuleName, $this->cursorName, $cursor);
    }
}
?>