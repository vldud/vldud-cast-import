<?
	
namespace Vldud\CastImport\DBHandler;

use \CIBlockElement as CIBlockElement;
use \COption as COption;
use \CPrice as CPrice;
	
class PriceUpdateBitrixDBHandler extends DBHandler
{

    private $cursorModuleName;
    private $cursorName;
    private $markup;

    function __construct($config = array())
    {
        $this->cursorModuleName = (isset($config["cursorModuleName"])) ? $config["cursorModuleName"] : "tega.import";
        $this->cursorName = (isset($config["cursorName"])) ? $config["cursorName"] : "import_cursor";
        $this->markup = (isset($config["markup"])) ? $config["markup"] : -5;
    }

    private function errorOutput($strMessage)
    {
        // echo "<pre>";
        // print_r($strMessage);
        // echo "</pre>";
        AddMessage2Log($strMessage);
    }

    public function getCode($str)
    {
        $code = Cutil::translit(
            $str,
            "ru",
            array(
                "replace_space" => "-",
                "replace_other" => "-"
            )
        );
        return $code;
    }

    public function getProductArray($data)
    {
        
		$arData = array();
		$arProps = array();

		/*$arProps["46"] = 9;*/

		$arData["IBLOCK_ID"] = 16;
		$arData["XML_ID"] = $data[0];
		$arData["PROPERTY_XML_ID"] = $data[0];
		$arData["PRICE"] = $data[3];

		$arData["PROPERTY_VALUES"] = $arProps;

		return $arData;
        
    }

    private function getPrice($price)
    {
        $markup = floatval($this->markup);

        $price = str_replace(" ", "", $price);
        $price = str_replace(",", ".", $price);
        $price = $price * (100 + $markup) / 100;
        /*$price = round($price, 2);*/
        $price = round($price);
        return floatval($price);
    }

    public function importProduct($arData)
    {
        $arProductArray = $arData;

        $res = CIBlockElement::GetList(
            Array(),
            Array(
                "IBLOCK_ID" => $arData["IBLOCK_ID"],
                "PROPERTY_XML_ID" => $arData["PROPERTY_XML_ID"]
            ),
            false,
            Array("nPageSize" => 1),
            Array("ID", "NAME")
        );
        $el = new CIBlockElement;
        $ob = $res->GetNextElement();
        if ($ob !== false) {
            $arFields = $ob->GetFields();
            $PRODUCT_ID = $arFields["ID"];

            $minPrice = sprintf("%.2f", $this->getPrice($arData["PRICE"]));
            CIBlockElement::SetPropertyValuesEx(
                $arFields["ID"],
                $arFields["IBLOCK_ID"],
                array("MINIMUM_PRICE" => $minPrice)
            );
            /*
            CIBlockElement::SetPropertyValuesEx(
				$arFields["ID"],
				$arFields["IBLOCK_ID"],
				array("DISCOUNT" => 9)
			);
            */
        }
		
        if ($PRODUCT_ID > 0) {
            
			$arFields = Array(
				"PRODUCT_ID" => $PRODUCT_ID,
				"CATALOG_GROUP_ID" => 1,
				"PRICE" => $this->getPrice($arData["PRICE"]),
				"CURRENCY" => "RUB"
			);
			$res = CPrice::GetList(
				array(),
				array(
					"PRODUCT_ID" => $PRODUCT_ID,
					"CATALOG_GROUP_ID" => 1
				)
			);
			if ($arr = $res->Fetch()) {
				CPrice::Update($arr["ID"], $arFields);
			} else {
				CPrice::Add($arFields);
			}
            
        }
    }

    public function getCursor()
    {
        return COption::getOptionString($this->cursorModuleName, $this->cursorName, 1);
    }

    public function setCursor($cursor = 1)
    {
        return COption::SetOptionString($this->cursorModuleName, $this->cursorName, $cursor);
    }
}

?>