<?

namespace Vldud\CastImport\Facade;
	
class SpoutImportFacade extends ImportFacade
{

    public function run()
    {
        $offset = $this->objDBHandler->getCursor();
		$isEndOfFile = true;
        $iterator = 0;
        /* $saveCursorIterator < $limit*/
        $saveCursorIterator = $this->config["saveCursorIterator"];
        $limit = $this->config["limit"];

        if($this->objFileHandler->xmlData) {

            foreach ($this->objFileHandler->xmlData->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $key => $row) {
                    $iterator++;
                    if ($iterator <= $offset) {
                        continue;
                    } else {
                        $arData = $this->objDBHandler->getProductArray($row);
                        if (is_array($arData)) {
                            $this->objDBHandler->importProduct($arData);
                        }
                        /* save cursor position every "$saveCursorIterator" times */
                        if (($iterator - $offset) % $saveCursorIterator == 0) {
                            $this->objDBHandler->setCursor($iterator);
                        }
                        if ($iterator == $limit + $offset) {
                            $isEndOfFile = false;
                            break;
                        }
                    }
                }
                break;
            }
            $this->objFileHandler->xmlData->close();

            if ($isEndOfFile) {
                $this->objDBHandler->setCursor();
                return true;
            }

        }
		
		return false;
    }
}	
?>