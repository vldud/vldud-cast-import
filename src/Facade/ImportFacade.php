<?
	
namespace Vldud\CastImport\Facade;
	
class ImportFacade
{

    public $objDBHandler;
    public $objFileHandler;
    public $config;

    public function __construct($objDBHandler, $objFileHandler, $config = false)
    {
        $this->objDBHandler = $objDBHandler;
        $this->objFileHandler = $objFileHandler;
        if ($config == false) {
            $config = array(
                "saveCursorIterator" => 1,
                "limit" => 3,
            );
        }
        $this->config = $config;
    }

    public function run()
    {

        $offset = $this->objDBHandler->getCursor();
        $iterator = 0;
        /* $saveCursorIterator < $limit*/
        $saveCursorIterator = $this->config["saveCursorIterator"];
        $limit = $this->config["limit"];

        foreach ($this->objFileHandler->xmlData as $offer) {

            $iterator++;
            if ($iterator <= $offset) {
                continue;
            } else {
                $arData = $this->objDBHandler->getProductArray($offer);
                echo "<pre>";
                print_r($arData);
                echo "</pre>";
                /*
                $this->objDBHandler->importProduct($arData);
                */
                /* save cursor position every "$saveCursorIterator" times */
                if (($iterator - $offset) % $saveCursorIterator == 0) {
                    $this->objDBHandler->setCursor($iterator);
                }
                if ($iterator == $limit + $offset)
                    break;
            }

        }

        if ($iterator == count($this->objFileHandler->xmlData)) {
            $this->objDBHandler->setCursor(0);
        }

    }

}
?>